package com.allstate.mongo.dao;

import com.allstate.mongo.entities.CompactDisc;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collection;

@SpringBootTest
public class CompactDiscRepoImplTest {
    @Autowired
    ICompactDiscData dao;

    @BeforeEach
    public void cleanUp() {
        dao.dropDb();
    }

    @Test
    public void testSaveCompactDiscDocument() {
        CompactDisc disc1 = new CompactDisc("Abba Gold", "Abba", 12.99);
        CompactDisc disc2 = new CompactDisc("True", "Spandau", 12.99);
        CompactDisc disc3 = new CompactDisc("Romantic Favourites", "Richard Clayderman", 12.99);

        dao.addCompactDisc(disc1);
        dao.addCompactDisc(disc2);
        dao.addCompactDisc(disc3);

        Collection<CompactDisc> discs = dao.getAllDiscs();
        discs.forEach(disc -> System.out.println(disc.getTitle()));
        Assert.assertEquals(3, discs.size());
    }
}
