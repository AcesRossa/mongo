package com.allstate.mongo.service;

import com.allstate.mongo.dao.ICompactDiscData;
import com.allstate.mongo.entities.CompactDisc;
import com.allstate.mongo.services.CompactDiscService;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CompactDiscServiceTest {
    @Autowired
    private CompactDiscService service;

    @Autowired
    ICompactDiscData dao;

    @AfterEach
    public void cleanUp() {
        dao.dropDb();
    }

    @Test
    public void addCompactDiscsThroughServiceLayer() {
        service.addCompactDisc(new CompactDisc("Chic", "Risque", 12.99));
        Assert.assertEquals(1, service.getAllDiscs().size());
    }
}
