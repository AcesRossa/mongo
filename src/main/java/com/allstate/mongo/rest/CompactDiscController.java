package com.allstate.mongo.rest;

import com.allstate.mongo.entities.CompactDisc;
import com.allstate.mongo.services.ICompactDiscService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class CompactDiscController {
    @Autowired
    ICompactDiscService service;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public Collection<CompactDisc> getAll() {
        return service.getAllDiscs();
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public void save(@RequestBody CompactDisc compactDisc) {
        service.addCompactDisc(compactDisc);
    }

    @RequestMapping(value = "/artist", method = RequestMethod.GET)
    public Collection<CompactDisc> getDiscsByArtist(@RequestParam String artist) {
        return service.getDiscsByArtist(artist);
    }

    @RequestMapping(value = "/title", method = RequestMethod.GET)
    public CompactDisc getDiscsByTitle(@RequestParam String title) {
        return service.getCompactDiscByTitle(title);
    }
}
