package com.allstate.mongo.dao;

import com.allstate.mongo.entities.CompactDisc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public class CompactDiscRepoImpl implements ICompactDiscData {
    @Autowired
    private MongoTemplate tpl;

    @Override
    public void addCompactDisc(CompactDisc compactDisc) {
        tpl.insert(compactDisc);
    }

    @Override
    public CompactDisc getCompactDiscByTitle(String title) {
        Query query = new Query();
        query.addCriteria(Criteria.where("title").is(title));
        return tpl.findOne(query, CompactDisc.class);
    }

    @Override
    public Collection<CompactDisc> getDiscsByArtist(String artist) {
        Query query = new Query();
        query.addCriteria(Criteria.where("artist").is(artist));
        return tpl.find(query, CompactDisc.class);
    }

    @Override
    public Collection<CompactDisc> getAllDiscs() {
        return tpl.findAll(CompactDisc.class);
    }

    @Override
    public void dropDb() {
        for(String collectionName: tpl.getCollectionNames()) {
            if(!collectionName.startsWith("system.")) {
                tpl.dropCollection(collectionName);
            }
        }
    }
}
