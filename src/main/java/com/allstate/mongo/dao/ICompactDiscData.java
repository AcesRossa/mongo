package com.allstate.mongo.dao;

import com.allstate.mongo.entities.CompactDisc;

import java.util.Collection;

public interface ICompactDiscData {
    void addCompactDisc(CompactDisc compactDisc);
    CompactDisc getCompactDiscByTitle(String title);
    Collection<CompactDisc> getDiscsByArtist(String artist);
    Collection<CompactDisc> getAllDiscs();
    void dropDb();
}
