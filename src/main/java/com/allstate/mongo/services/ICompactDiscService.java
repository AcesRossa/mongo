package com.allstate.mongo.services;

import com.allstate.mongo.entities.CompactDisc;

import java.util.Collection;

public interface ICompactDiscService {
    Collection<CompactDisc> getAllDiscs();
    Collection<CompactDisc> getDiscsByArtist(String artist);
    CompactDisc getCompactDiscByTitle(String title);
    void addCompactDisc(CompactDisc compactDisc);
    void dropDb();
}
