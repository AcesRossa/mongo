package com.allstate.mongo.services;

import com.allstate.mongo.dao.ICompactDiscData;
import com.allstate.mongo.entities.CompactDisc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class CompactDiscService implements ICompactDiscService {
    @Autowired
    ICompactDiscData dao;

    @Override
    public Collection<CompactDisc> getAllDiscs() {
        return dao.getAllDiscs();
    }

    @Override
    public Collection<CompactDisc> getDiscsByArtist(String artist) {
        return dao.getDiscsByArtist(artist);
    }

    @Override
    public CompactDisc getCompactDiscByTitle(String title) {
        return dao.getCompactDiscByTitle(title);
    }

    @Override
    public void addCompactDisc(CompactDisc compactDisc) {
        dao.addCompactDisc(compactDisc);
    }

    @Override
    public void dropDb() {
        dao.dropDb();
    }
}
